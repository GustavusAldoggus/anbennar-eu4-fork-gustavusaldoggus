government = tribal
add_government_reform = monstrous_horde
government_rank = 1
primary_culture = gray_orc
religion = great_dookan
technology_group = tech_orcish
national_focus = DIP
capital = 740

1421.1.1 = {
	monarch = {
		name = "Bras�r"
		dynasty = "Frozenmaw"
		birth_date = 1399.10.4
		adm = 6
		dip = 4
		mil = 4
	}
}