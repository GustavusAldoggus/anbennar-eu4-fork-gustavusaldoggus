government = monarchy
add_government_reform = autocracy_reform
government_rank = 2
primary_culture = sun_elf
religion = bulwari_sun_cult
technology_group = tech_elven
national_focus = DIP
capital = 496
historical_friend = F32 #historical subject

1000.1.1 = { set_country_flag = mage_organization_centralized_flag }