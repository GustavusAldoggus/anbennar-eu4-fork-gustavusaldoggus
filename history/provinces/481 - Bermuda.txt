# No previous file for Bermuda
owner = F09
controller = F09
add_core = F09
culture = sandfang_gnoll
religion = xhazobkult

hre = no

base_tax = 1
base_production = 1
base_manpower = 2

trade_goods = livestock

capital = ""

is_city = yes


discovered_by = tech_salahadesi
discovered_by = tech_gnollish