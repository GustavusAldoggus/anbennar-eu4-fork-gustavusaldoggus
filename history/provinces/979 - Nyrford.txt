# No previous file for Nyrford
owner = Z14
controller = Z14
add_core = Z14
culture = olavish
religion = skaldhyrric_faith

hre = no

base_tax = 1
base_production = 1
base_manpower = 1

trade_goods = fur

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_gnomish
discovered_by = tech_giantkind