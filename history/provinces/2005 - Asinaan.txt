# No previous file for Asinaan
owner = H11
controller = H11
add_core = H11
culture = tuathak
religion = eordellon
capital = ""
fort_15th = yes

hre = no

base_tax = 2
base_production = 1
base_manpower = 1

trade_goods = chinaware

native_size = 14
native_ferocity = 6
native_hostileness = 6