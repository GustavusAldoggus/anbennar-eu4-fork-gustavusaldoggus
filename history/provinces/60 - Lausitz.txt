#60 - Lausitz | East Saltmarsh

owner = A12
controller = A12
add_core = A12
culture = imperial_halfling
religion = regent_court
hre = yes
base_tax = 4
base_production = 7
trade_goods = salt
base_manpower = 2
capital = "East Saltmarsh"
is_city = yes
fort_15th = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_kobold

add_permanent_province_modifier = {
	name = widderoy_estuary_modifier
	duration = -1
}