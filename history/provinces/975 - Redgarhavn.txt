# No previous file for Redgarhavn
owner = Z13
controller = Z13
add_core = Z13
culture = olavish
religion = skaldhyrric_faith

hre = no

base_tax = 4
base_production = 4
base_manpower = 3

trade_goods = naval_supplies
center_of_trade = 1

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_gnomish
discovered_by = tech_giantkind